#pragma once
#include "baidu_face_api.h"

#ifdef BAIDUFACEDLL_EXPORTS
#define BAIDUFACEDLL_API __declspec(dllexport)
#else
#define BAIDUFACEDLL_API __declspec(dllimport)
#endif

EXTERN_C BAIDUFACEDLL_API int Add(int a, int b);
EXTERN_C BAIDUFACEDLL_API BaiduFaceApi* Init();
EXTERN_C BAIDUFACEDLL_API void UnInit(BaiduFaceApi* api);
EXTERN_C BAIDUFACEDLL_API int GetGroupList(BaiduFaceApi* api, char** res);
EXTERN_C BAIDUFACEDLL_API int UserAdd(BaiduFaceApi* api, const char* user_id, const char* image, char** res);
EXTERN_C BAIDUFACEDLL_API int UserDelete(BaiduFaceApi* api, const char* user_id, char** res);
EXTERN_C BAIDUFACEDLL_API int Identifier(BaiduFaceApi* api, const char* imgstr, char** res);
EXTERN_C BAIDUFACEDLL_API int RgbLivenessCheck(BaiduFaceApi* api, const char* imgstr, char** res);