﻿namespace FaceIdentifier
{
    public class FaceRecord
    {
        public string personSN { get; set; }
        public string personName { get; set; }
        public string createTime { get; set; }
        public string image { get; set; }

        public FaceRecord() { }

        public FaceRecord(string personSN, string personName, string createTime, string image)
        {
            this.personSN = personSN;
            this.personName = personName;
            this.createTime = createTime;
            this.image = image;
        }
    }
}
