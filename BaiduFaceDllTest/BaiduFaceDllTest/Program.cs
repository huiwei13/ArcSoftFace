﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BaiduFaceDllTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IntPtr Init = BaiduFaceDll.Init();
            Console.WriteLine(Init);

            //StringBuilder sb_getGroupList = new StringBuilder(500);
            //int GetGroupList = BaiduFaceDll.GetGroupList(sb_getGroupList);
            //Console.WriteLine(GetGroupList);
            //Console.WriteLine(sb_getGroupList.ToString());

            string base64;
            Bitmap bmp1 = new Bitmap("1001.jpg");
            using (MemoryStream ms = new MemoryStream())
            {
                bmp1.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] arr1 = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(arr1, 0, (int)ms.Length);
                ms.Close();
                base64 = Convert.ToBase64String(arr1);
            }

            StringBuilder sb = new StringBuilder(1000);
            sb.Clear();
            int delete = BaiduFaceDll.UserDelete(Init,"1001", ref sb);
            Console.WriteLine("delete:" + delete);
            Console.WriteLine("delete:" + sb.ToString());

            sb.Clear();
            int UserAdd = BaiduFaceDll.UserAdd(Init,"1001", base64,ref sb);
            Console.WriteLine("add:" + UserAdd);
            Console.WriteLine("add:" + sb.ToString());

            for(int s = 0; s < 1000; s++)
            {
                Console.WriteLine("第n次，n="+s);
                sb.Clear();
                int Identifier = BaiduFaceDll.Identifier(Init,base64, ref sb);
                Console.WriteLine(Identifier);
                Console.WriteLine(sb.ToString());
            }
            BaiduFaceDll.UnInit(Init);
            Console.WriteLine("退出测试");
        }
    }
}
