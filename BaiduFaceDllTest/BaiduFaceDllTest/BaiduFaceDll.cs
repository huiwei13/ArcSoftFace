﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BaiduFaceDllTest
{
    class BaiduFaceDll
    {
        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Add(int v1, int v2);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr Init();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UnInit(IntPtr api);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserAdd(IntPtr api, string user_id, string image, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetGroupList(IntPtr api, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserDelete(IntPtr api, string user_id, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Identifier(IntPtr api, string imgstr, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int RgbLivenessCheck(IntPtr api, string imgstr, ref StringBuilder res);
    }
}
